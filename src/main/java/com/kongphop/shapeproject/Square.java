/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.shapeproject;

/**
 *
 * @author Admin
 */
public class Square {
    private double x;
    public Square(double x) {
        this.x = x;
    }
    public double calArea() {
        return x * x;
    }
    public double getX() {
        return x;
    }
     public void setX(double x) {
        if(x<=0) {
            System.out.println("Error: Length must more than zero!!!");
            return;
        }
        this.x = x;
    }
}
