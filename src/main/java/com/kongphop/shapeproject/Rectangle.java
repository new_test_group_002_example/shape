/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.shapeproject;

/**
 *
 * @author Admin
 */
public class Rectangle {
    private double x, y;
    public Rectangle(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public double calArea() {
        return x * y;
    }
    public double getX() {
        return x;
        
    }
    public void setX(double x) {
        if(x<=0) {
            System.out.println("Error: Width must more than zero!!!");
            return;
        }
        this.x = x;
    }
    public double getY() {
        return y;
    }
     public void setY(double y) {
        if(y<=0) {
            System.out.println("Error: Length must more than zero!!!");
            return;
        }
        this.y = y;
    }
}
