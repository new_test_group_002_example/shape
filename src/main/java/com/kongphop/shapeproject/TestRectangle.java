/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.shapeproject;

/**
 *
 * @author Admin
 */
public class TestRectangle {
    public static void main(String[] args) {
         Rectangle rectangle1 = new Rectangle(3, 5);
         System.out.println("Area of rectangle1"
            + "(x = "+ rectangle1.getX()+")"
            + "(y = "+ rectangle1.getY()+") is " 
            + rectangle1.calArea());
         rectangle1.setX(2);
         rectangle1.setY(3);
         System.out.println("Area of rectangle1"
            + "(x = "+ rectangle1.getX()+")"
            + "(y = "+ rectangle1.getY()+") is " 
            + rectangle1.calArea());
         rectangle1.setX(0);
         rectangle1.setY(0);
         System.out.println("Area of rectangle1"
            + "(x = "+ rectangle1.getX()+")"
            + "(y = "+ rectangle1.getY()+") is " 
            + rectangle1.calArea());
            
    } 
}
